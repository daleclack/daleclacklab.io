// Process for tab buttons
function change_page(evt, PageName) {
    var i, tabContent, tabLinks;

    // Use "tabcontent" class to hide the pages
    tabContent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabContent.length; i++) {
        tabContent[i].style.display = "none";
    }

    // Delete "active" class for tablinks
    tabLinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tabLinks.length; i++) {
        tabLinks[i].className = tabLinks[i].className.replace("active", "");
    }

    // Show current tab page, and add "active" class for the button
    document.getElementById(PageName).style.display = "block";
    evt.currentTarget.className += " active";
}

document.getElementById("btn_main").click();
const width = screen.width;
console.log(width);
if (width < 900) {
    // On mobile devices, delete "center" class for table
    document.getElementById("tab_pages").setAttribute("colspan", "2");
    var main_table1 = document.getElementById("main_table");
    main_table1.className = main_table1.className.replace("center", "");
}